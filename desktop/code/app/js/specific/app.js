(function($) {

	(function() {

		if ( $('.c-preloader-progress').length ) {

			Pace.on('progress', function(progress){

				progress = Math.round(progress)

				$('.c-preloader-progress__bar').css({
					width: progress + '%'
				});

				$('.c-preloader-progress__bar_vert').css({
					height: progress + '%'
				});

				$('.c-preloader-progress__number').text(progress);

			});

			Pace.on('done', function(){

				$('body').addClass('_loaded');
				$('.c-preloader-progress').addClass('b-preloader_done');

			});
		}
	})();

	$(document).ready(function() {

		(function() {

			if ( $('.c-slick').length ) {

				$('.c-slick').slick({
					speed: 300,
					slidesToShow: 1,

					adaptiveHeight: true,
					centerMode: true,
					variableWidth: true,
					infinite: false,
					draggable: false,
					swipe: false,

					nextArrow: $('.c-slick__next'),
					prevArrow: $('.c-slick__prev'),

					responsive: [
						{
							breakpoint: 980,
							settings: {
								slidesToShow: 1,
								variableWidth: false,
								centerPadding: 0
							}
						}
					]
				});
			}
		})();

		(function() {

			if ( $('.c-slick_fade').length ) {

				$('.c-slick_fade').slick({
					fade: true,
					speed: 300,
					slidesToShow: 1,

					adaptiveHeight: true,
					centerMode: true,
					variableWidth: true,
					infinite: false,
					draggable: false,
					swipe: false,

					nextArrow: $('.c-slick_fade__next'),
					prevArrow: $('.c-slick_fade__prev'),

					responsive: [
						{
							breakpoint: 980,
							settings: {
								slidesToShow: 1,
								variableWidth: false,
								centerPadding: 0
							}
						}
					]
				});
			}
		})();

		(function() {

			if ( $('.c-switcher----hide-info, .c-switcher----more-info').length ) {

				$('.c-switcher----hide-info, .c-switcher----more-info').click(function(event) {

					$('.c-slick').slick('setPosition');
				});
			}
		})();

		(function() {

			if ( $('.c-slick_3_slides').length ) {

				$('.c-slick_3_slides').slick({
					speed: 300,
					slidesToShow: 3,

					adaptiveHeight: true,
					mobileFirst: true,
					infinite: false,

					nextArrow: $('.c-slick_3_slides__next'),
					prevArrow: $('.c-slick_3_slides__prev')
				});
			}
		})();
	});

	$(window).load(function() {

		(function() {

			if ( $('.c-size').length ) {

				initProperti();

				$(window).resize(function(event) {

					initProperti();
				});

				function initProperti() {

					$('.c-size').each(function(index, el) {

						var $parent	= $(this);

						$('.c-size__item').each(function(index, el) {

							var properties	= $(this).data('property').split(', ');

							for (var i = 0; i < properties.length; i++) {

								$(this).css( properties[i], $parent.css( properties[i] ) );
							}
						});
					});
				}
			}
		})();

		(function() {

			if ( $('.c-circle').length ) {

				$('.c-circle').circleProgress({
					size: 100,
					startAngle: -1.57,
					thickness: 4,
					fill: {
						color: '#49cbf2'
					},
					emptyFill: '#f5f8fa'
				});
			}
		})();

		(function() {

			if ( $('.c-move-tracking').length ) {

				$('.c-move-tracking').each(function(index, el) {

					var $this			= $(this),
						className		= $this.data('class'),
						firstInit		= $this.data('first-init'),
						firstInitDelay	= $this.data('first-init-delay') || 0,
						timerActive		= false,
						time			= $this.data('time') || 5000,
						timeout001;

					console.log(firstInitDelay, time)

					if ( firstInit ) {

						setTimeout( init, firstInitDelay );
					}

					$this.on('mousemove touchmove', function(event) {

						if ( timerActive != true) {

							init();

						} else {

							reinit();
						}

					});

					function init() {

						timerActive = true;

						if ( $this.hasClass(className) ) {

							$this.removeClass(className);
						}

						timeout001 = setTimeout( destroy, time );
					}

					function reinit() {

						clearTimeout(timeout001);
						timeout001 = setTimeout( destroy, time );
					}

					function destroy() {

						if ( !$this.hasClass(className) ) {

							timerActive = false;
							$this.addClass(className);
						}
					}
				});
			}
		})();

		(function() {

			if ( $('.c-table-hover').length ) {

				$('.c-table-hover th, .c-table-hover td').hover(

					function(){

						var index = $(this).index() + 1;

						$('.c-table-hover th:nth-child(' + index + ')').addClass('-ol-table__th_vert_hover');
						$('.c-table-hover td:nth-child(' + index + ')').addClass('-ol-table__td_vert_hover');

						$(this).siblings().add( $(this) ).addClass('-ol-table__th_horiz_hover');
					},

					function(){

						var index = $(this).index() + 1;

						$('.c-table-hover th:nth-child(' + index + ')').removeClass('-ol-table__th_vert_hover');
						$('.c-table-hover td:nth-child(' + index + ')').removeClass('-ol-table__td_vert_hover');

						$(this).siblings().add( $(this) ).removeClass('-ol-table__th_horiz_hover');
					}
				);
			}
		})();

		(function() {

			if ( $('.c-modal').length ) {

				$('.c-modal').modal({
					overlayBg: 'rgba(34, 38, 46, .4)'
				});
			}
		})();

		(function() {

			if ( $('.c-modal_style_2').length ) {

				$('.c-modal_style_2').modal({
					overlayBg: 'rgba(245, 248, 250, 0.8)'
				});
			}
		})();

		(function() {

			if ( $('select.c-select').length ) {

				$('select.c-select').select2({
					placeholder: 'Select an option',
					theme: 'fm',
					width: '100%'
				});

				$("select.c-select").on("select2:open", function() {
					$(".select2-search__field").attr("placeholder", "Search...");
				});

				$("select.c-select").on("select2:close", function() {
					$(".select2-search__field").attr("placeholder", null);
				});
			}
		})();

		(function() {

			if ( $('.c-switcher').length ) {

				$('.c-switcher').each(function(index, el) {

					var $this = $(this);

					$this.on('click', function(event) {

						addingDataAttribute($this);
					});

					function addingDataAttribute($this) {
					
						if ( $this.data('class') && $this.data('element') ) {

							var element		= $this.data('element'),
								className	= $this.data('class');

							$(element).toggleClass(className);
						}
					};
				});
			}
		})();

		(function() {

			if ( $('.c-switcher-checkbox').length ) {

				$('.c-switcher-checkbox').each(function(index, el) {

					var $this = $(this);

					addingDataAttribute($this);

					$this.on('change', function(event) {

						console.log(1)

						addingDataAttribute($this);
					});

					function addingDataAttribute($this) {

						if ( $this.data('element') ) {

							var element		= $this.data('element');

							if ( $this.data('class') ) {

								var className		= $this.data('class');

								$(element).toggleClass(className);

							} else if ( $this.data('name') ) {

								var name		= $this.data('name');

								$(element).attr( 'data-' + name, $this.is(':checked') );
							}
						}
					};
				});
			}
		})();
	});

})(jQuery);