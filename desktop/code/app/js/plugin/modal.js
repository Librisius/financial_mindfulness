(function($){

	jQuery.fn.modal = function(options){

		// BEGIN plagin options

		options = $.extend({
			boxCenter: true,
			overlayBg: 'rgba(0, 0, 0, 0.6)',
			boxzIndex: 995
		}, options);

		// END plagin options

		var wFlagBox,
			hFlagBox,
			$block				= $(this),
			$body				= $('body'),
			$box 				= $block.find('.c-modal__box'),
			$boxWrap			= $block.find('.c-modal__box-wrapper'),
			$inner	 			= $block.find('.c-modal__inner'),
			$boxOverlay			= $block.find('.c-modal__overlay'),
			$boxClose			= $block.find('.c-modal__close'),
			$boxCloseNoAnchor	= $('.c-modal__close_noanchor'),
			$boxBind 			= $('.c-modal__bind'),

			boxCenter			= options.boxCenter,
			boxzIndex			= options.boxzIndex,
			overlayBg			= options.overlayBg;

		$box.each(function(){

			var $thisBox = $(this);

			wFlagBox = true;
			hFlagBox = true;

			$block.css({
				position: 'fixed',
				left: '-100%',
				top: '-100%',
				zIndex: boxzIndex,
				opacity: 0,
				visibility: 'hidden',
				width: '100%',
				height: '100%',
				overflow: 'auto'
			});

			$inner.css({
				display: 'table',
				tableLayout: 'fixed',
				width: '100%',
				height: '100%',
				backgroundColor: overlayBg,
			});

			$boxOverlay.css({
				width: '100%',
				height: '100%',
				position: 'absolute',
				left: 0,
				top: 0
			});

			$boxClose
				.add($boxCloseNoAnchor)
				.add($boxBind)
				.css({
					cursor: 'pointer'
				});

			$thisBox.css({
				position: 'relative',
				zIndex: 1
			});

			$boxWrap.css({
				display: 'table-cell'
			});

			if ( boxCenter ) {

				$boxWrap.css({
					verticalAlign: 'middle'
				});

				$thisBox.css({
					margin: '0 auto'
				});
			}
		});
		
		$boxBind.click(function() {

			var index 			= $(this).attr('data-number'),
				$thisBoxWrap	= $block.filter('[data-number="' + index + '"]'),
				$thisBox		= $thisBoxWrap.find('.c-modal__box'),
				attribut		= $thisBoxWrap.attr('data-attribut');

			if (attribut) {

				$body.attr('data-attribut', attribut);
			}

			$body.addClass('_modal-active');

			$block.filter('[data-number="' + ( $(this).attr('data-number') ) + '"]').css({
				'visibility': 'visible',
				left: 0,
				top: 0
			}).fadeTo(500, 1).fadeIn();
		});

		$boxCloseNoAnchor.click(function() {

			var index 			= $(this).attr('data-number')
				$thisBoxWrap	= $block.filter('[data-number="' + index + '"]');

			closeBox($thisBoxWrap);
		});

		$boxClose.click(function() {
			
			closeBox( $(this).closest( $block ) );
		});

		function closeBox($this) {

			var index 			= $this.attr('data-number'),
				$thisBoxWrap	= $block.filter('[data-number="' + index + '"]');

			$thisBoxWrap.fadeOut();
			$body.removeAttr('data-attribut');
			$body.removeClass('_modal-active');

		}
	}
	
})(jQuery);