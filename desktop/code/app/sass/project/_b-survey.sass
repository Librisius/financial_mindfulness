.b-survey
	$per_hover:			2%
	$per_active:		2%

	$bg_c_extremely:	$brand-danger
	$bg_c_quite:		$brand-danger-middle
	$bg_c_moderately:	$brand-middle
	$bg_c_little:		$brand-primary-middle
	$bg_c_not:			$brand-primary

	$m_l_btn_style_2:	50px

	$h-answer-btn_mb:	64px

	position: relative
	text-align: center

	+row($h: 400px)

	+md-mobile

		+row_set()

	&_style_2
		height: auto

	&_style_3_mb

		+md-mobile
			position: relative
			display: table
			min-height: calc( 100vh - #{$h-b-header + $h-b-heading_sm - $brd_w + $h-b-heading + $h-b-nav-mb} )

	&__content

	&_style_3_mb &__content

		+md-mobile
			display: table-cell
			vertical-align: middle

			height: 100%

			padding-bottom: $h-answer-btn_mb

	&_style_3_mb &__answer-wrapper

		+md-mobile
			position: absolute
			bottom: 0
			left: 0
			z-index: 1

	&__answer-wrapper

	&__step
		position: absolute
		top: 40px
		left: 0

		width: 100%

		margin: -10px 0 -11px

		color: $gray-light
		font-weight: 300
		line-height: 2

		+md-mobile
			position: static
			top: auto
			left: auto

			padding-top: 40px

		&_active
			margin: -15px 0 -22px
			color: $brand-primary
			font-size: $font-size-xxxl

	&__step > &__step_active
		display: inline-block
		margin: -5px 0 -11px

	&__question
		padding: 64px $w_full-col 0

		+md-mobile
			padding: 40px $m-col_tb 45px

	&_style_3_mb &__question

		+md-mobile
			padding: 40px $m-col_tb

	&__question-heading
		margin: -10px 0 29px // 40 - 11 = 29

		color: $c-h
		font-size: $font-size-medium
		font-weight: 600
		line-height: 2

		+md-mobile
			margin-bottom: 24px // 35 - 11 = 24

	&__question-text
		margin: -11px 0 -15px

		color: $gray
		font-size: $font-size-xl
		font-weight: 300
		line-height: 2

		+md-mobile
			margin: -12px 0 -13px
			font-size: $font-size-medium
			line-height: 2.25

	&__answer
		position: relative
		overflow: hidden

		+md-mobile
			background-color: $gray-lightest

		&:before
			$h: 10px

			content: ''

			position: absolute
			top: -$h
			left: 0

			height: $h
			width: 100%

			background-color: $white
			box-shadow: 0 0 5px rgba($black, .3)

			+md-mobile
				display: none

	&_style_2 &__answer
		margin: 40px 0 40px -#{$m_l_btn_style_2}

		+md-tablet
			margin-left: 0

		&:before
			display: none

	&_style_2 &__answer_first
		margin-top: 0

	&_style_2 &__answer_last
		margin-bottom: 0

	&__answer-inner

	&_style_2 &__answer-inner

		+inline( $item: '-item', $valign: middle )

		+md-tablet

			+inline_set( $item: '-item' )
			+cell( $item: '-item', $valign: middle )

	&__answer-inner-item

	&__answer-btn
		height: 70px
		transition: background-color .2s
		cursor: pointer

		+md-mobile
			height: $h-answer-btn_mb

		&_extremely
			background-color: $bg_c_extremely

			+md-mobile
				background-color: transparent

			&:hover
				background-color: darken($bg_c_extremely, $per_hover)

				+md-mobile
					background-color: transparent

			&:active
				background-color: darken($bg_c_extremely, $per_hover + $per_active)

				+md-mobile
					background-color: transparent

		&_quite
			background-color: $bg_c_quite

			+md-mobile
				background-color: transparent

			&:hover
				background-color: darken($bg_c_quite, $per_hover)

				+md-mobile
					background-color: transparent

			&:active
				background-color: darken($bg_c_quite, $per_hover + $per_active)

				+md-mobile
					background-color: transparent

		&_moderately
			background-color: $bg_c_moderately

			+md-mobile
				background-color: transparent

			&:hover
				background-color: darken($bg_c_moderately, $per_hover)

				+md-mobile
					background-color: transparent

			&:active
				background-color: darken($bg_c_moderately, $per_hover + $per_active)

				+md-mobile
					background-color: transparent

		&_little
			background-color: $bg_c_little

			+md-mobile
				background-color: transparent

			&:hover
				background-color: darken($bg_c_little, $per_hover)

				+md-mobile
					background-color: transparent

			&:active
				background-color: darken($bg_c_little, $per_hover + $per_active)

				+md-mobile
					background-color: transparent

		&_not
			background-color: $bg_c_not

			+md-mobile
				background-color: transparent

			&:hover
				background-color: darken($bg_c_not, $per_hover)

				+md-mobile
					background-color: transparent

			&:active
				background-color: darken($bg_c_not, $per_hover + $per_active)

				+md-mobile
					background-color: transparent

	&_style_2 &__answer-btn
		$d: 80px

		display: block // tag a

		width: $d
		height: $d

		margin-left: $m_l_btn_style_2
		border-radius: round($d/2)
		padding: 10px

		font-size: $font-size-xs
		font-weight: 600
		line-height: 2

		+md-tablet
			margin: 0 auto

	&__answer-btn-text
		margin: -4px 0 -6px

		color: $white
		font-weight: 600

		text-align: center
		text-align: -webkit-center // for burron tag in Chrome
		white-space: nowrap

		+md-mobile
			margin: 0
			font-size: 0

		&:after
			font-size: 0

			+md-mobile
				font-size: 34px

	&_style_2 &__answer-btn-text
		margin: -6px 0 -9px

	&__answer-btn_extremely &__answer-btn-text

		@extend .i-smile-1

		+md-mobile
			color: $bg_c_extremely

	&__answer-btn_extremely:hover &__answer-btn-text

		+md-mobile
			color: darken($bg_c_extremely, $per_hover)

	&__answer-btn_extremely:active &__answer-btn-text

		+md-mobile
			color: darken($bg_c_extremely, $per_hover + $per_active)

	&__answer-btn_quite &__answer-btn-text

		@extend .i-smile-2

		+md-mobile
			color: $bg_c_quite

	&__answer-btn_quite:hover &__answer-btn-text

		+md-mobile
			color: darken($bg_c_quite, $per_hover)

	&__answer-btn_quite:active &__answer-btn-text

		+md-mobile
			color: darken($bg_c_quite, $per_hover + $per_active)

	&__answer-btn_moderately &__answer-btn-text

		@extend .i-smile-3

		+md-mobile
			color: $bg_c_moderately

	&__answer-btn_moderately:hover &__answer-btn-text

		+md-mobile
			color: darken($bg_c_moderately, $per_hover)

	&__answer-btn_moderately:active &__answer-btn-text

		+md-mobile
			color: darken($bg_c_moderately, $per_hover + $per_active)

	&__answer-btn_little &__answer-btn-text

		@extend .i-smile-4

		+md-mobile
			color: $bg_c_little

	&__answer-btn_little:hover &__answer-btn-text

		+md-mobile
			color: darken($bg_c_little, $per_hover)

	&__answer-btn_little:active &__answer-btn-text

		+md-mobile
			color: darken($bg_c_little, $per_hover + $per_active)

	&__answer-btn_not &__answer-btn-text

		@extend .i-smile-5

		+md-mobile
			color: $bg_c_not

	&__answer-btn_not:hover &__answer-btn-text

		+md-mobile
			color: darken($bg_c_not, $per_hover)

	&__answer-btn_not:active &__answer-btn-text

		+md-mobile
			color: darken($bg_c_not, $per_hover + $per_active)